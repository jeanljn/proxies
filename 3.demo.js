// imaginons un pêcheur qui décrit ce qu'il a pêché dernièrement
const breton = {
	thon: 67,
	merlan: 45,
	cabillaud: 22,
	hareng: 34,
	requin: null,
	decrire(nomDuPoisson) {
		const debut = `Le dernier ${nomDuPoisson} que j'ai pêché ?`
		if (this[nomDuPoisson]) return `${debut} Il faisait ${this[nomDuPoisson]} centimètres`
		return `${debut} Euh, ben j'en ai jamais pêché en fait`
	}
}

// imaginons ce même pêcheur, mais avec une petite tendance à gonfler les chiffres
// le constructeur de Proxy attend 2 params : l'objet à "proxifier" et le handler
const marseillais = new Proxy(breton, {
	// le handler définit les traps, c'est à dire les actions qui seront interceptées
	get: (vincent, poisson) => {
		if (typeof vincent[poisson] === 'number') return vincent[poisson] + 30
		return vincent[poisson]
	}
})

// et hop, ce même pêcheur devient soudainement anglais
const anglais = new Proxy(breton, {
	get: (vincent, prop) => {
		if (prop !== 'decrire') return vincent[prop]
		// une méthode, c'est jamais qu'une propriété de type fonction
		return (fishName) => {
			const intro = `Oh? The last "${fishName}" I catched, you ask?`
			if (vincent[fishName]) return `${intro} Well, if memory serves well, 'twas a hefty ${Math.round(vincent[fishName] / 2.54)} inches long`
			return `${intro} Actually, I don't recall I e'er caught one, eh`
		}
	}
})

console.log(breton.decrire('thon')) // "Le dernier thon que j'ai pêché ? Il faisait 67 centimètres"
console.log(marseillais.decrire('thon')) // "Le dernier thon que j'ai pêché ? Il faisait 97 centimètres"
console.log(anglais.decrire('thon')) // `Oh? The last "thon" I catched, you ask? Well, if memory serves well, 'twas a hefty 26 inches long`

marseillais.cabillaud = 86

// logique
console.log(marseillais.decrire('cabillaud')) // "Le dernier cabillaud que j'ai pêché ? Il faisait 116 centimètres"
// mais
console.log(breton.decrire('cabillaud')) // "Le dernier cabillaud que j'ai pêché ? Il faisait 86 centimètres"
// => le proxy est un intermédiaire pour TOUTES les interactions avec l'objet
// ce sont 2 objets distincts, mais si une interaction avec le proxy n'est pas redéfinie par son handler, c'est l'objet cible qui répondra
breton !== marseillais // true, mais
breton.decrire === marseillais.decrire // also true, par contre
breton.decrire !== anglais.decrire // car dans le handler de l'anglais, decrire est intercepté et remplacé par une autre fonction

// NB: get n'est pas le seul "piège"

const autreMarseillais = new Proxy(breton, {
	// set interceptera les opérations d'écriture
	set: (vincent, poisson, taille) => {
		vincent[poisson] = taille - 30
	}
})

autreMarseillais.cabillaud = 86

console.log(breton.decrire('cabillaud')) // "Le dernier cabillaud que j'ai pêché ? Il faisait 56 centimètres"

const bretonFier = new Proxy(breton, {
	deleteProperty: (vincent, poisson) => {
		if (vincent[poisson] > 100) {
			console.log("Jamais je n'oublierai un poisson aussi gros")
			return
		}
		delete vincent[poisson]
	}
})

delete bretonFier.hareng // la prop vaut 34 => elle est supprimée
console.log(bretonFier.decrire('hareng')) // "Le dernier hareng que j'ai pêché ? Euh, ben j'en ai jamais pêché en fait"

bretonFier.hareng = 115
console.log(bretonFier.decrire('hareng')) // "Le dernier hareng que j'ai pêché ? Il faisait 115 centimètres"
delete bretonFier.hareng // "Jamais je n'oublierai un poisson aussi gros"
console.log(bretonFier.decrire('hareng')) // "Le dernier hareng que j'ai pêché ? Il faisait 115 centimètres"

// TLDR: un Proxy est un objet intermédiaire qui transmet toutes ses interactions à un objet cible par défaut
// le handler se charge de redéfinir une ou plusieurs interactions et leur comportement