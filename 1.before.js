const snakeToCamel = (snakeString) =>
	snakeString.split('_').map((word, index) => {
		if (index === 0) {
			return word
		}
		return word[0].toUpperCase() + word.slice(1)
	})
	.join('')

const camelToPascal = (camelString) =>
	camelString[0].toUpperCase() + camelString.slice(1)

const baseballToPascal = (baseballString) =>
	baseballString.split('⚾').map((word) => {
		return word[0].toUpperCase() + word.slice(1)
	})
	.join('')

const pascalToBaseball = (pascalString) =>
	pascalString.split(/(?=[A-Z])/)
		.join('⚾')
		.toLowerCase()

module.exports = {
	snakeToCamel,
	camelToPascal,
	baseballToPascal,
	pascalToBaseball,
}