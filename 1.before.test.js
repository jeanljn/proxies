const {
	snakeToCamel,
	camelToPascal,
	baseballToPascal,
	pascalToBaseball,
} = require('./1.before')

const camelSentences = ['iAmSingingInTheRain', 'dontInvestInNASDAQForNow', 'bazinga']

const snakeSentences = ['i_am_singing_in_the_rain', 'dont_invest_in_NASDAQ_for_now', 'bazinga']

const pascalSentences = ['IAmSingingInTheRain', 'DontInvestInNASDAQForNow', 'Bazinga']

const kebabSentences = ['i-am-singing-in-the-rain', 'dont-invest-in-NASDAQ-for-now', 'bazinga']

const baseballSentences = ['i⚾am⚾singing⚾in⚾the⚾rain', 'dont⚾invest⚾in⚾NASDAQ⚾for⚾now', 'bazinga']

describe('Case conversion/detection', () => {
	it('converts snake case to camel case', () => {
		const conversions = snakeSentences.map(snakeToCamel)

		expect(conversions).toEqual(camelSentences)
	})

	it('converts camel case to pascal case', () => {
		const conversions = camelSentences.map(camelToPascal)

		expect(conversions).toEqual(pascalSentences)
	})

	it('converts baseball case to pascal case', () => {
		const conversions = baseballSentences.map(baseballToPascal)

		expect(conversions).toEqual(pascalSentences)
	})

	it.skip('converts pascal case to baseball case', () => {
		const conversions = pascalSentences.map(pascalToBaseball)

		expect(conversions).toEqual(baseballSentences)
	})

	it.skip('cannot convert from kebab case', () => {
		throw new Error('no available function')
	})
})