const {
	snakeToCamel,
	camelToPascal,
	baseballToPascal,
	pascalToBaseball,
	kebabToCamel,
	kebabToSnake,
	kebabToPascal,
	kebabToBaseball,
} = require('./4.after')

const camelSentences = ['iAmSingingInTheRain', 'dontInvestInNASDAQForNow', 'bazinga']

const snakeSentences = ['i_am_singing_in_the_rain', 'dont_invest_in_NASDAQ_for_now', 'bazinga']

const pascalSentences = ['IAmSingingInTheRain', 'DontInvestInNASDAQForNow', 'Bazinga']

const kebabSentences = ['i-am-singing-in-the-rain', 'dont-invest-in-NASDAQ-for-now', 'bazinga']

const baseballSentences = ['i⚾am⚾singing⚾in⚾the⚾rain', 'dont⚾invest⚾in⚾NASDAQ⚾for⚾now', 'bazinga']

describe('Case conversion/detection', () => {
	it('converts snake case to camel case', () => {
		const conversions = snakeSentences.map(snakeToCamel)

		expect(conversions).toEqual(camelSentences)
	})

	it('converts camel case to pascal case', () => {
		const conversions = camelSentences.map(camelToPascal)

		expect(conversions).toEqual(pascalSentences)
	})

	it('converts baseball case to pascal case', () => {
		const conversions = baseballSentences.map(baseballToPascal)

		expect(conversions).toEqual(pascalSentences)
	})

	it('converts pascal case to baseball case', () => {
		const conversions = pascalSentences.map(pascalToBaseball)

		expect(conversions).toEqual(baseballSentences)
	})

	it('converts from kebab case to anything', () => {
		const snakes = kebabSentences.map(kebabToSnake)
		const camels = kebabSentences.map(kebabToCamel)
		const pascals = kebabSentences.map(kebabToPascal)
		const baseballs = kebabSentences.map(kebabToBaseball)

		expect(snakes).toEqual(snakeSentences)
		expect(camels).toEqual(camelSentences)
		expect(pascals).toEqual(pascalSentences)
		expect(baseballs).toEqual(baseballSentences)
	})
})