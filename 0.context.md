# La conversion de casse

La conversion de casse, c'est pas fun à coder mais c'est utile (interactions entre votre langage de prédilection et SQL, génération de XML destiné à Windows ou de documents répondant à des normes strictes de syntaxe, etc.). Voyons si on peut au moins rendre ça plus facile.

## En quoi c'est pas fun ?

Ce qui n'est pas fun dans cette conversion, c'est que si vous voulez couvrir une grande diversité de casses (camel, snake, kebab, etc.), il faut coder un nombre toujours croissant de fonctions. Par exemple, pour couvrir 5 casses différentes, il vous faudra 20 fonctions (5 casses en entrée * 4 casses en sortie, inutile de coder un `camelToCamel` par exemple).

En codant ces fonctions une par une, vous risquez d'écrire du code qui se ressemble assez pour vous suggérer une factorisation, mais pas suffisamment pour que la factorisation soit facile. Jetez un oeil aux fichiers `1.before.*` pour mieux cerner le problème.

NB: Dans cet exemple et dans les suivants, on va considérer une cinquième casse (bien bien) fictive, le _baseball⚾case_.

Dans les tests, on voit vite la limite de cette implémentation : il faut penser à écrire chaque cas de conversion, depuis toutes les casses, vers toutes les autres casses. Et puis, quand on vous demande soudainement de trouver un moyen de préserver les acronymes en majuscules (ex: pour éviter que `iLoveLJN` ne devienne `i_love_l_j_n`), c'est la goutte de trop. Dans le cas présent, c'est 20 fonctions qu'il faudrait modifier.

## La solution : prendre du recul

Certes, pour passer de camelCase à PascalCase, il suffit de changer la casse de la première lettre. Mais c'est un cas particulier.

Prenons le cas d'une conversion de snake à camel. Si on fait ce process "à la main", on se rend compte qu'on réfléchit en termes de _mots_. Notre cerveau découpe la phrase `ceci_est_une_phrase_pas_trop_longue` pour en faire "ceci est une phrase pas trop longue". Puis on va mentalement coller ces mots et les flanquer d'une majuscule : `ceciEstUnePhrasePasTropLongue`.

Si notre cerveau passe intuitivement par cette étape intermédiaire, pourquoi ne pas la reproduire dans notre code ? C'est en fait quelque chose de très courant, qu'on retrouve notamment dans la NLP : la _tokenisation_ et son dérivé, les _tokens_. Les tokens, ce sont des mots, rien de plus. Si on considère que les fonctions `snakeToCamel` et `snakeToKebab` démarrent toutes les deux par la même étape de tokenisation (`snakeTo*`), on se rend compte qu'on pourrait isoler cette étape et créer une fonction qui prend une phrase en snake_case en entrée, et génère un array de tokens en sortie. Même réflexe pour les fonctions `camelToKebab` et `snakeToKebab` qui terminent toutes les deux (`*ToKebab`) par une étape de... "détokenisation" ? (appelons-la simplement "conversion"). On peut donc isoler une fonction qui attend un array de tokens en entrée et qui retourne une phrase en kebab-case en sortie. En prime, le `To` nous sert de repère pour identifer les fonctions à utiliser.

Avec cette réflexion, tout devient plus simple. Jetez un oeil à `2.rationalization.js` pour voir ce que ça donne concrètement.

## L'astuce : la classe Proxy

La documentation du MDN n'est pas forcément ultra-limpide sur ce à quoi sert cette classe. D'autant qu'en français, l'utilisation de ce mot se limite plus ou moins à NordVPN (qui ne sponsorise pas cette présentation, rassurez-vous). Mais en anglais, on l'utilise pour désigner un intermédiaire. Dans le cas d'une procuration pour un vote, on dit du bénéficiaire de la procuration qu'il est votre proxy. Si vous êtes mandataire d'une personne dépendante, on parle aussi de proxy. Pour les budgets nationaux divisés entre les régions, elles-mêmes chargées de redistribuer ces budgets aux différents départements ou aux communes, on dit de ces régions que ce sont des proxy.

La classe Proxy en JS a exactement cette fonction : elle s'intercale entre :
- un objet, une fonction ou à peu près n'importe quel "truc" avec lequel on peut interagir,
- et nous, qui nous apprêtons à interagir avec ce sujet, ou n'importe quel dispositif interagissant avec l'objet, pas nécessairement un humain.

Le Proxy n'agit pas seul, il est associé à un _handler_. Si le Proxy déclenche les événements liés aux interactions mentionnées ci-dessus, c'est le _handler_ qui dicte comment y répondre.

Jetez un oeil à `3.demo.js` pour mieux cerner son champ d'action.

## Concrètement, ça donne quoi ?

Puisque les méthodes de conversion suivent un formalisme particulier `xxxToYyy`, on peut définir un handler qui intercepte tout accès à une propriété, étudie le nom de la propriété, identifie `xxx` et `Yyy` et compose la fonction résultante. Ainsi, un appel à `camelToSnake` sera intercepté, la fonction de tokenisation `camel` sera appelée pour générer les tokens, et la fonction `snake` transformera ces mêmes tokens en une string en snake_case.

On a le handler, mais sur quoi va-t-on définir le proxy ? Eh bien, sur un objet vide. Ici, ce qu'on veut exporter, c'est une simple collection de méthodes. Mais toutes ces méthodes vont être générées à la volée par le handler à partir d'un petit ensemble de _tokenizers_ et de _converters_ (qu'on n'a pas besoin d'exporter car inutiles en tant que tel). Il n'y a donc rien de "physique" à exporter, aucune méthode hardcodée. Tout ça se passe dans les fichiers `4.after.*`.

Ah et comme tout le process s'est "industrialisé", il est devenu beaucoup plus simple de gérer la préservation des acronymes, qui s'opère tout le temps au niveau des tokens. C'est donc chose faite : tous les tests passent !

## Waouh, et après ?

J'espère que vous avez maintenant réalisé le potentiel des proxies (oui, ça se pluralise comme ça). Pour autant, ils restent cantonnés à des cas d'usage très précis et assez limités. Vous n'allez pas pouvoir en mettre partout (en tout cas, ça n'aura pas spécialement d'intérêt). Mais quand on identifie qu'un proxy peut nous faciliter la tâche, en général, ça va aussi faciliter la maintenance du code, l'ajout de fonctionnalités, la détection de bugs etc.

Par exemple, il sera nettement plus simple de modifier notre module de conversion de casse pour qu'il prenne en charge des arrays et des objets. Et plus besoin de répéter 20 fois les modifs en adaptant à chaque logique, on code ça au niveau du handler.

Un petit aperçu du futur de l'humanité dans `5.beyond.js`