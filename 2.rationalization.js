const lower = token => token.toLowerCase()

const tokenizers = {
	snake: (input) => input.split('_'),
	camel: (input) => input.split(/(?=[A-Z][^A-Z])|(?<=[^A-Z])(?=[A-Z])/),
	pascal: (input) => input.split(/(?=[A-Z][^A-Z])|(?<=[^A-Z])(?=[A-Z])/),
	kebab: (input) => input.split('-'),
	baseball: (input) => input.split('⚾'),
}

const converters = {
	snake: (tokens) => tokens.join('_'),
	camel: (tokens) => tokens.map((token, index) => index ? token[0].toUpperCase() + token.slice(1) : token).join(''),
	pascal: (tokens) => tokens.map((token) => token[0].toUpperCase() + token.slice(1)).join(''),
	kebab: (tokens) => tokens.join('-'),
	baseball: (tokens) => tokens.join('⚾'),
}

// ex: snakeToCamel = snake (tokenizer) To (repère) Camel (converter)
// => snake(snakeString) retourne un array de tokens (= de mots)
// => on .map(lower) pour tout passer en minuscules, tant pis pour les acronymes
// => camel(tokens) retransforme les tokens en une phrase camelCase
const snakeToCamel = (snakeString) =>
	converters.camel(tokenizers.snake(snakeString).map(lower))

const camelToSnake = (camelString) =>
	converters.snake(tokenizers.camel(camelString).map(lower))

// c'est encore un peu long à écrire mais au moins, on peut le faire à coups de c/c
module.exports = {
	snakeToCamel,
	camelToSnake,
	// ...
}