const preserveAllCapsWords = token => {
	if (token.length > 1 && token === token.toUpperCase())
		return token
	return token.toLowerCase()
}

const tokenizers = {
	snake: (input) => input.split('_'),
	camel: (input) => input.split(/(?=[A-Z][^A-Z])|(?<=[^A-Z])(?=[A-Z])/),
	pascal: (input) => input.split(/(?=[A-Z][^A-Z])|(?<=[^A-Z])(?=[A-Z])/),
	kebab: (input) => input.split('-'),
	baseball: (input) => input.split('⚾'),
}

const converters = {
	snake: (tokens) => tokens.join('_'),
	camel: (tokens) => tokens.map((token, index) => index ? token[0].toUpperCase() + token.slice(1) : token).join(''),
	pascal: (tokens) => tokens.map((token) => token[0].toUpperCase() + token.slice(1)).join(''),
	kebab: (tokens) => tokens.join('-'),
	baseball: (tokens) => tokens.join('⚾'),
}

// la nouveauté est ici
// une fois qu'on a notre fonction de conversion qui marche sur une string
// on va la transformer en une plus grosse fonction qui gère arrays et objects
const recursivize = (atomicFunction) => (something) => {
	if (typeof something === 'string') {
		return atomicFunction(something)
	}
	
	if (Array.isArray(something)) {
		return something.map((subthing) => recursivize(atomicFunction)(subthing))
	}

	if (typeof something !== 'object') return something // ou throw, au choix

	return Object.fromEntries(
		Object.entries(something).map(([key, val]) => [atomicFunction(key), recursivize(atomicFunction)(val)])
	)
}

module.exports = new Proxy({}, {
	get: (_, method) => {
		const [left, right] = method.split('To').map((fn) => fn.toLowerCase())
		
		const tokenize = tokenizers[left]
		const convert = converters[right]

		if (!tokenize || !convert || tokenize === convert) return undefined

		const atom = (phrase) => convert(tokenize(phrase).map(preserveAllCapsWords))

		return recursivize(atom)
	}
})